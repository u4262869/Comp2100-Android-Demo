package edu.anu.comp2100.comp2100_android_demo;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class PersistenceActivity extends AppCompatActivity {

    EditText textEntered;
    TextView textSaved;
    static final String filename = "comp2100";
    FileInputStream inputStream;
    FileOutputStream outputStream;
    File persistentFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_persistence);

        textSaved = (TextView) findViewById(R.id.text_saved);
        textEntered = (EditText) findViewById(R.id.text_entry);

        /* Read data from the persistent file */
        persistentFile = new File(getFilesDir(),filename);
        if (persistentFile.exists()){
            try{
                inputStream = openFileInput(filename);
                BufferedReader input = new BufferedReader(new InputStreamReader(inputStream));
                String line;
                StringBuilder buffer = new StringBuilder();
                while ((line = input.readLine()) != null){
                    buffer.append(line);
                }
                textSaved.setText(buffer.toString());
            }catch(Exception e){
                e.printStackTrace();
            }
        }
    }

    public void saveText(View view){
        String textBuffer = textSaved.getText().toString();
        String appendText = textEntered.getText().toString();
        textBuffer += "..";
        textBuffer += appendText;
        try {
            outputStream = openFileOutput(filename, Context.MODE_PRIVATE);
            outputStream.write(textBuffer.getBytes());
            outputStream.close();
            textSaved.setText(textBuffer);
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}

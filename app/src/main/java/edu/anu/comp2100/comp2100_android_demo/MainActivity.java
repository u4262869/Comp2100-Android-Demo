package edu.anu.comp2100.comp2100_android_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    public final static String EXTRA_MESSAGE = "edu.anu.comp2100.comp2100_android_demo.MESSAGE";
    EditText textMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textMessage = (EditText) findViewById(R.id.text_message);
    }

    /* Click handler for launching Counter Activity */
    public void launchCounterActivity(View view) {
        Intent launchCounterIntent = new Intent(this, CounterActivity.class);
        startActivity(launchCounterIntent);
    }

    public void launchSimpleActivity(View view){
        Intent launchSimpleIntent = new Intent(this, SimpleActivity.class);
        String message = textMessage.getText().toString();
        launchSimpleIntent.putExtra(EXTRA_MESSAGE, message);
        startActivity(launchSimpleIntent);
    }

    public void launchPersistenceActivity(View view){
        Intent launchPersIntent = new Intent(this, PersistenceActivity.class);
        startActivity(launchPersIntent);
    }
}


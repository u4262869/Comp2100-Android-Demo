package edu.anu.comp2100.comp2100_android_demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class SimpleActivity extends AppCompatActivity {

    /* Declare widget objects */
    TextView textStatic;
    TextView textDynamic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_simple);

        /* Instantiate widgets */
        textStatic = (TextView) findViewById(R.id.text_static);
        textDynamic = (TextView) findViewById(R.id.text_dynamic);

        /* Get Intent Message */
        Intent simpleIntent = getIntent();
        String message = simpleIntent.getStringExtra(MainActivity.EXTRA_MESSAGE);
        textDynamic.setText(message);
    }
}

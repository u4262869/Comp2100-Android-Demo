package edu.anu.comp2100.comp2100_android_demo;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class CounterActivity extends AppCompatActivity {

    TextView counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_counter);
        counter = (TextView) findViewById(R.id.counter);
    }

    public void incrementHandler(View view){
        int value = Integer.parseInt(counter.getText().toString());
        value += 1;
        counter.setText(Integer.toString(value));
    }

    public void decrementHandler(View view){
        int value = Integer.parseInt(counter.getText().toString());
        value -= 1;
        counter.setText(Integer.toString(value));
    }

}

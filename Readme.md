# COMP 2100/2500/6442: Android Programming

## Lecture on 11/03/2016 

This project contains code demonstrated during the lecture.

The concepts covered include:

* Creating multiple Activities
* Launching Activities using Intents
* Passing messages between Activities using Intents
* Input Controls - Buttons, EditText
* Click Handlers for Buttons - onClick()
* Widgets - TextView
* Changing widget contents
* Persistent Data using Files in Internal Storage
